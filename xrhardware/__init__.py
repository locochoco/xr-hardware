#!/usr/bin/env python3 -i
# Copyright 2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""XR hardware database and associated utilities."""
